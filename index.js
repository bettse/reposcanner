
var GitHubApi = require("github");
var github = new GitHubApi({
  debug: !!process.env.DEBUG,
  headers: {
    "user-agent": "RepoScanner"
  },
  Promise: require('bluebird'),
});

github.authenticate({
  type: 'token',
  token: process.env.GH_TOKEN
});

github.repos.getForOrg({
  org: process.env.GH_ORG,
}, withRepoList);

function withRepoList(err, res) {
  if (err) {
    console.error(err);
    return;
  }
  res.forEach(perRepo);
  if (github.hasNextPage(res)) {
    github.getNextPage(res, res.headers, withRepoList);
  }
}

function perRepo(repo) {
  github.repos.getContent({
    user: process.env.GH_ORG,
    repo: repo.name,
    path: '.eslintrc'
  }, withFile);
}

function withFile(err, file) {
  if (err) {
    checkFile(null);
    return;
  }
  if (file.type != 'file') {
    console.error("Not a file!");
    return;
  }
  switch (file.encoding) {
    case 'base64':
      checkFile(new Buffer(file.content, 'base64').toString('ascii'));
      break;
    default:
      console.error("Unknown encoding", file.encoding);
      break;
  }
}

function checkFile(file) {
  if (file) {
    try {
      eslint = JSON.parse(file);
    } catch (err) {
      console.log("Error parsing file", file);
      return;
    }
    if (eslint.extends === 'eslint-config-invision') {
      console.log("Extends");
      if (eslint.rules && eslint.rules.quotes) {
        console.log("Quotes override");
      }
    } else {
      console.log("Custom rules");
    }
  } else {
    console.log("No file");
  }
}

